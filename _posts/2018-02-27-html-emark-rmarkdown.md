---
cover: coverU.jpg
layout: post
title:  "Document: El Estudio del Proceso de Busqueda de Productos en Linea Usando Metodologia DELPHY"
date:   2018-02-27 11:32:14 -0300
crawlertitle: "Taller 2"
summary: "Taller 2"
categories: jekyll hwarch posts
tags: ['front-end']
excerpt_separator: <!--more-->
---

<p style='color:#214063'>
Automatically detect anomalies in the operation.
</p>
<!--more-->

<!-- load remote readme file from github -->
{% include https://gitlab.com/siste11/ProductOnlineMarkets/raw/master/siste11_taller2.html %}
