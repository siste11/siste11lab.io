---
layout: post
title: Repositorio de Sistemas Expertos
cover: coverU.jpg
date:   2018-02-10 12:00:00
categories: posts
---

## Biblioteca Sistemas Expertos

Repositorio de proyectos de Sistemas Expertos

## Otras Comunidades en los Repositorios:

- [Arquitectura del Software](http://arsoft12.gitlab.io/)
  - [Documentos](http://arsoft12.gitlab.io/posts/)
  - [Presentaciones](http://arsoft12.gitlab.io/slide/)
- [Arquitectura del Hardware](http://arquh11.gitlab.io/)
  - [Documentos](http://arquh11.gitlab.io/posts/)
  - [Presentaciones](http://arquh11.gitlab.io/slide/)
- [Sistemas Expertos](http://siste11.gitlab.io/)
  - [Documentos](http://siste11.gitlab.io/posts/)
  - [Presentaciones](http://siste11.gitlab.io/slide/)

