---
layout: slide
title: "Presentation: El Estudio del Proceso de Busqueda de Productos en Linea Usando Metodologia DELPHY"
date:   2018-02-11 15:33:15 -0300
description: Article on markdown format.
categories: posts presentation
theme: black
transition: slide
excerpt_separator: <!--more-->
---

<p style='color:#214063'>
Automatically detect anomalies in the operation.
</p>

<!--more-->

<section data-background="https://gitlab.com/iush/hardware-architecture/raw/master/images/background.png" data-markdown data-separator="^----" data-separator-vertical="^->" >
<!-- load remote readme file from github -->
{% remote_markdown https://gitlab.com/siste11/se_mark/raw/master/Readme.md %}
</section>
